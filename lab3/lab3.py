import os
from databaseController import databaseController
from myError import MyError

def getCommands():
    print("1  - insert new teacher\n"
        "2  - delete teacher\n"
        "3  - update teacher\n"
        "4  - insert student\n"
        "5  - delete student\n"
        "6  - update student\n"
        "7  - insert subject\n"
        "8  - delete subject\n"
        "9  - update subject\n"
        "10 - insert group\n"
        "11 - delete group\n"
        "12 - update group\n"
        "13 - insert mark\n"
        "14 - delete mark\n"
        "15 - update mark\n"
        "16 - get all commands\n"
        "17 - quit")

database = databaseController()

command = ""
getCommands()
while command != "17":
    command = input("Enter your command: ")
    os.system("cls")
    try:
        if command == "1":
            database.insertTeacher()
            print("Teacher was added successfully")
        elif command == "2":
            if(database.deleteTeacherById()):
                print("Teacher was deleted successfully")
            else:
                print("No teacher with such id")
        elif command == "3":
            if(database.updateTeacherById()):
                print("Teacher was updated successfully")
            else:
                print("No teacher with such id")
        elif command == "4":
            database.insertStudent()
            print("Student was added successfully")
        elif command == "5":
            if(database.deleteStudentById()):
                print("Student was deleted successfully")
            else:
                print("No student with such id")
        elif command == "6":
            if(database.updateStudentById()):
                print("Student was updated successfully")
            else:
                print("No student with such id")
        elif command == "7":
            database.insertSubject()
            print("Subject was added successfully")
        elif command == "8":
            if(database.deleteSubjectByName()):
                print("Subject was deleted successfully")
            else:
                print("No subject with such id")
        elif command == "9":
            if(database.updateSubjectByName()):
                print("Subject was updated successfully")
            else:
                print("No subject with such id")
        elif command == "10":
            database.insertGroup()
            print("Group was added successfully")
        elif command == "11":
            if(database.deleteGroupByName()):
                print("Group was deleted successfully")
            else:
                print("No group with such id")
        elif command == "12":
            if(database.updateGroupByName()):
                print("Group was updated successfully")
            else:
                print("No group with such id")
        elif command == "13":
            database.insertMark()
            print("Mark was added successfully")
        elif command == "14":
            if(database.deleteMarkById()):
                print("Mark was deleted successfully")
            else:
                print("No mark with such id")
        elif command == "15":
            if(database.updateMarkById()):
                print("Mark was updated successfully")
            else:
                print("No mark with such id")
        elif command == "16":
            getCommands()
        elif command == "17":
            print("Program finished!")
        else:
            print("Wrong command")
    except MyError as err:
        print(err.text)