import database
import os
import funcs

from time import time
from psycopg2 import errors
from sqlalchemy.exc import IntegrityError, DatabaseError
from myError import MyError
from data import subjects, teachers, students, marks, groups

class databaseController:

    def __init__(self):
        self.database = database.Database()

    def insertTeacher(self):
        os.system("cls")
        name = funcs.getName("Enter new teacher's name: ")
        age = funcs.getPosNum("Enter teacher's age: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        teacher = teachers.Teacher(name, age, subject)
        try:
            self.database.insertTeacher(teacher)
        except IntegrityError:
            raise MyError("ERROR! No subjects in table 'subjects' like ({})".format(subject))
        except DatabaseError:
            raise MyError("ERROR! Teacher ({}({})) should be older than 18".format(name, age))

    def deleteTeacherById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter teacher's id: ")
        return self.database.deleteTeacherById(id)

    def updateTeacherById(self):
        
        os.system("cls")
        id = funcs.getPosNum("Enter teacher's id: ")
        name = funcs.getName("Enter new teacher's name: ")
        age = funcs.getPosNum("Enter teacher's age: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        teacher = teachers.Teacher(name, age, subject, id)
        try:
            return self.database.updateTeacherById(teacher)
        except IntegrityError:
            raise MyError("ERROR! No subjects in table 'subjects' like ({})".format(subject))

    def insertStudent(self):
        os.system("cls")
        name = funcs.getName("Enter new student's name: ")
        email = funcs.getMail("Enter student's email: ")
        group = funcs.getGroupName("Enter student's group name: ")
        student = students.Student(name, email, group)
        try:
            self.database.insertStudent(student)
        except IntegrityError:
            raise MyError("ERROR! No groups in table 'groups' like ({})".format(group))
    
    def deleteStudentById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter student's id: ")
        return self.database.deleteStudentById(id)

    def updateStudentById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter student's id: ")
        name = funcs.getName("Enter new student's name: ")
        email = funcs.getMail("Enter student's email: ")
        group = funcs.getGroupName("Enter student's group name: ")
        student = students.Student(name, email, group, id)
        try:
            return self.database.updateStudentById(student)
        except IntegrityError:
            raise MyError("ERROR! No groups in table 'groups' like ({})".format(group))

    def insertSubject(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter new subject name: ")
        hours = funcs.getPosNum("Enter subject's hours: ")
        subject = subjects.Subject(name, hours)
        try:
            self.database.insertSubject(subject)
        except IntegrityError:
            raise MyError("ERROR! This subject({}) is already exists!".format(name))
    
    def deleteSubjectByName(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter subject's name: ")
        return self.database.deleteSubjectByName(name)
    
    def updateSubjectByName(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter subject name: ")
        hours = funcs.getPosNum("Enter subject's hours: ")
        subject = subjects.Subject(name, hours)
        return self.database.updateSubjectByName(subject)

    def insertGroup(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        class_num = funcs.getPosNum("Enter class number: ")
        curator = funcs.getPosNum("Enter curator's id: ")
        group = groups.Group(name, class_num, curator)
        try:
            self.database.insertGroup(group)
        except IntegrityError:
            raise MyError(("ERROR! Group({}) is already exists or teacher with id({}) is already a curator!!!"
                            "\nOR\nERROR! No teacher with id - {}").format(name, curator, curator))

    def deleteGroupByName(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        return self.database.deleteGroupByName(name)

    def updateGroupByName(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        class_num = funcs.getPosNum("Enter class number: ")
        curator = funcs.getPosNum("Enter curator's id: ")
        group = groups.Group(name, class_num, curator)
        try:
            return self.database.updateGroupByName(group)
        except IntegrityError:
            raise MyError(("ERROR! Teacher with id({}) is already a curator!!!"
                            "\nOR\nERROR! No teacher with id - {}").format(curator, curator))

    def insertMark(self):
        os.system("cls")
        student_id = funcs.getPosNum("Enter student's id: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        mark = 101
        while mark > 100:
            mark = funcs.getPosNum("Enter mark from 1 to 100: ")
        date = funcs.getDate("Enter date {dd/mm/yyyy}: ")
        mark = marks.Mark(student_id, subject, mark, date)
        try:
            self.database.insertMark(mark)
        except IntegrityError:
            raise MyError("ERROR! No student with id({}) or no subject({})".format(student_id, subject))

    def deleteMarkById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter mark's id: ")
        return self.database.deleteMarkById(id)        

    def updateMarkById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter marks's id: ")
        student_id = funcs.getPosNum("Enter student's id: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        mark = 101
        while mark > 100:
            mark = funcs.getPosNum("Enter mark from 1 to 100: ")
        date = funcs.getDate("Enter date {dd/mm/yyyy}: ")
        mark = marks.Mark(student_id, subject, mark, date, id)
        try:
            return self.database.updateMarkById(mark)
        except IntegrityError:
            raise MyError("ERROR! No student with id({}) or no subject({})".format(student_id, subject))
