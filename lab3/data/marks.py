from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, Date, ForeignKey
from .subjects import Subject
from .students import Student

Base = declarative_base()

class Mark(Base):
    __tablename__ = "marks"
    id = Column(Integer, primary_key=True, nullable=False)
    subject = Column(Text, ForeignKey(Subject.name), nullable=False)
    student_id = Column(Integer, ForeignKey(Student.id), nullable=False)
    mark = Column(Integer, nullable=False)
    date = Column(Date, nullable=False)

    def __init__(self, student_id, subject, mark, date, id = 0):
        self.student_id = student_id
        self.subject = subject
        self.mark = mark
        self.date = date
        self.id = id