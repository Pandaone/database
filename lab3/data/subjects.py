from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text

Base = declarative_base()

class Subject(Base):
    __tablename__ = "subjects"
    name = Column(Text, primary_key=True, unique=True, nullable=False)
    hours = Column(Integer, nullable=False)

    def __init__(self, name, hours):
        self.name = name
        self.hours = hours