from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, ForeignKey
from .teachers import Teacher

Base = declarative_base()

class Group(Base):
    __tablename__ = "groups"
    group_name = Column(Text, primary_key=True, unique=True, nullable=False)
    class_num = Column(Integer, nullable=False)
    curator_id = Column(Integer, ForeignKey(Teacher.id), unique=True)

    def __init__(self, group_name, class_num, curator_id):
        self.group_name = group_name
        self.class_num =  class_num
        self.curator_id = curator_id