from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, ForeignKey
from .subjects import Subject

Base = declarative_base()

class Teacher(Base):
    __tablename__ = 'teachers'
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(Text, nullable=False)
    age = Column(Integer)
    subject = Column(Text, ForeignKey(Subject.name))

    def __init__(self, name, age, subject, id = None):
        self.id = id
        self.name = name
        self.age = age
        self.subject = subject