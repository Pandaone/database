from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, ForeignKey
from .groups import Group

Base = declarative_base()

class Student(Base):
    __tablename__ = "students"
    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(Text, nullable=False)
    email = Column(Text)
    group_name = Column(Text, ForeignKey(Group.group_name))

    def __init__(self, name, email, group_name,  id = None):
        self.name = name
        self.email = email
        self.group_name = group_name
        self.id = id