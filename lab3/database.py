import psycopg2
import sqlalchemy
from data import teachers, students, subjects, groups, marks

class Database:
    def __init__(self, *args):
        self.engine = sqlalchemy.create_engine('postgresql://postgres:Artem1011@localhost:5432/lab2')
        Session = sqlalchemy.orm.sessionmaker(bind=self.engine)
        self.session = Session()

    def __del__(self):
        self.session.close()      

    def insertTeacher(self, teacher):
        self.session.add(teacher)
        self.session.commit()

    def deleteTeacherById(self, teacherId):
        self.session.query(groups.Group).filter(groups.Group.curator_id == teacherId)\
        .update({groups.Group.curator_id: None})

        amount = self.session.query(teachers.Teacher).filter(teachers.Teacher.id == teacherId).delete()
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def updateTeacherById(self, teacher):
        amount = self.session.query(teachers.Teacher).filter(teachers.Teacher.id == teacher.id)\
        .update({teachers.Teacher.name: teacher.name, teachers.Teacher.age: teacher.age, 
        teachers.Teacher.subject: teacher.subject})
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def insertSubject(self, subject):
        self.session.add(subject)
        self.session.commit()
    
    def deleteSubjectByName(self, subjectName):
        self.session.query(teachers.Teacher).filter(teachers.Teacher.subject == subjectName)\
        .update({teachers.Teacher.subject: None})

        self.session.query(marks.Mark).filter(marks.Mark.subject == subjectName).delete()

        amount = self.session.query(subjects.Subject).filter(subjects.Subject.name == subjectName).delete()
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def updateSubjectByName(self, subject):
        amount = self.session.query(subjects.Subject).filter(subjects.Subject.name == subject.name)\
        .update({subjects.Subject.hours: subject.hours})
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False
    
    def insertStudent(self, student):
        self.session.add(student)
        self.session.commit() 
    
    def deleteStudentById(self, studentId):
        self.session.query(marks.Mark).filter(marks.Mark.student_id == studentId).delete()

        amount = self.session.query(students.Student).filter(students.Student.id == studentId).delete()
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False
    
    def updateStudentById(self, student):
        amount = self.session.query(students.Student).filter(students.Student.id == student.id)\
        .update({students.Student.name: student.name, students.Student.email: student.email, 
        students.Student.group_name: student.group_name})
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False
    
    def insertMark(self, mark):
        self.session.add(mark)
        self.session.commit()     

    def deleteMarkById(self, markId):
        amount = self.session.query(marks.Mark).filter(marks.Mark.id == markId).delete()
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def updateMarkById(self, mark):
        amount = self.session.query(marks.Mark).filter(marks.Mark.id == mark.id)\
        .update({marks.Mark.student_id: mark.student_id, marks.Mark.subject: mark.student_id, 
        marks.Mark.mark: mark.mark, marks.Mark.date: mark.date})
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def insertGroup(self, group):
        self.session.add(group)
        self.session.commit()    

    def deleteGroupByName(self, group_name):
        self.session.query(students.Student).filter(students.Student.group_name == group_name)\
        .update({students.Student.group_name: None})

        amount = self.session.query(groups.Group).filter(groups.Group.group_name == group_name).delete()
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False

    def updateGroupByName(self, group):
        amount = self.session.query(groups.Group).filter(groups.Group.group_name == group.group_name)\
        .update({groups.Group.class_num: group.class_num, groups.Group.curator_id: group.curator_id})
        self.session.commit()
        if amount > 0:
            return True
        else:
            return False
