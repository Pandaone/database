import os
from databaseController import databaseController
from myError import MyError

def getCommands():
    print("1  - insert new teacher\n"
        "2  - delete teacher\n"
        "3  - update teacher\n"
        "4  - insert student\n"
        "5  - delete student\n"
        "6  - update student\n"
        "7  - insert subject\n"
        "8  - delete subject\n"
        "9  - update subject\n"
        "10 - insert group\n"
        "11 - delete group\n"
        "12 - update group\n"
        "13 - insert mark\n"
        "14 - delete mark\n"
        "15 - update mark\n"
        "16 - generate students\n"
        "17 - generate marks\n"
        "18 - get all students that's curator teachers some subject and their name consists some pattern\n"
        "19 - get all students that have the most amount of marks in some interval\n"
        "20 - get average mark in some classroom\n"
        "21 - get all commands\n"
        "22 - quit")

database = databaseController()

command = ""
getCommands()
while command != "22":
    command = input("Enter your command: ")
    os.system("cls")
    try:
        if command == "1":
            if(database.insertTeacher()):
                print("Teacher was added successfully")
            else:
                print("Teacher wasn't added")
        elif command == "2":
            if(database.deleteTeacherById()):
                print("Teacher was deleted successfully")
            else:
                print("No teacher with such id")
        elif command == "3":
            if(database.updateTeacherById()):
                print("Teacher was updated successfully")
            else:
                print("No teacher with such id")
        elif command == "4":
            if(database.insertStudent()):
                print("Student was added successfully")
            else:
                print("Student wasn't added")
        elif command == "5":
            if(database.deleteStudentById()):
                print("Student was deleted successfully")
            else:
                print("No student with such id")
        elif command == "6":
            if(database.updateStudentById()):
                print("Student was updated successfully")
            else:
                print("No student with such id")
        elif command == "7":
            if(database.insertSubject()):
                print("Subject was added successfully")
            else:
                print("Subject wasn't added")
        elif command == "8":
            if(database.deleteSubjectByName()):
                print("Subject was deleted successfully")
            else:
                print("No subject with such id")
        elif command == "9":
            if(database.updateSubjectByName()):
                print("Subject was updated successfully")
            else:
                print("No subject with such id")
        elif command == "10":
            if(database.insertGroup()):
                print("Group was added successfully")
            else:
                print("Group wasn't added")
        elif command == "11":
            if(database.deleteGroupByName()):
                print("Group was deleted successfully")
            else:
                print("No group with such id")
        elif command == "12":
            if(database.updateGroupByName()):
                print("Group was updated successfully")
            else:
                print("No group with such id")
        elif command == "13":
            if(database.insertMark()):
                print("Mark was added successfully")
            else:
                print("Mark wasn't added")
        elif command == "14":
            if(database.deleteMarkById()):
                print("Mark was deleted successfully")
            else:
                print("No mark with such id")
        elif command == "15":
            if(database.updateMarkById()):
                print("Mark was updated successfully")
            else:
                print("No mark with such id")
        elif command == "16":
            if(database.generateStudents()):
                print("Students were generated successfully")
            else:
                print("Students weren't generated")
        elif command == "17":
            if(database.generateMarks()):
                print("Marks were generated successfully")
            else:
                print("Marks weren't generated")
        elif command == "18":
            os.system("cls")
            res = database.getStudentsByCuratorAndName()
            students = res[0]
            if len(students) == 0:
                print("No students were found")
                continue
            for i in range(len(students)):
                print("Student {}:( Name - {}, Email - {}, Group - {})".format(
                        i + 1, students[i][0], students[i][1], students[i][2]))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "19":
            os.system("cls")
            res = database.getStudentsByTheMostMarks()
            students = res[0]
            if len(students) == 0:
                print("No students were found")
                continue
            for i in range(len(students)):
                print("Student {}:\nName - {}\nEmail - {}\nGroup - {}\nAmount - {}\n".format(
                        i + 1, students[i][0], students[i][1], students[i][2], students[i][3]))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "20":
            res = database.getClassMarks()
            mark = res[0][0][0]
            if not mark:
                print("No students in this classroom")
                continue
            print("The average mark is {}".format(mark))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "21":
            getCommands()
        elif command == "22":
            print("Program finished!")
        else:
            print("Wrong command")
    except MyError as err:
        print(err.text)
