import re
import os
import datetime
from random import choice

def getName(str):
    os.system("cls")
    while True:
        name = input(str)
        os.system("cls")
        if re.match(r"[A-Z][a-z]+$", name):
            return name
        else:
            print("Wrong format!")

def getPosNum(str):
    os.system("cls")
    while True:
        try:
            num = int(input(str))
            os.system("cls")
            if num > 0:
                return num
            else:
                print("Number should be decimal and bigger then zero!")
        except ValueError:
            os.system("cls")
            print("Wrong format!")
        
def getSubjectName(str):
    os.system("cls")
    while True:
        os.system("cls")
        subject = input(str)
        if re.match(r"[A-Za-z]+$", subject):
            return subject
        else:
            print("Wrong format!")

def getGroupName(str):
    os.system("cls")
    while True:
        group = input(str)
        os.system("cls")
        if re.match(r"[A-Z]{2}-[0-9]{2}$", group):
            return group
        else:
            print("Wrong format! Group name should be like 'AA-00'")

def getMail(str):
    os.system("cls")
    while True:
        email = input(str)
        os.system("cls")
        if re.match(r"[\w_\.-]+@[a-z]+\.[a-z]+", email):
            return email
        else:
            print("Wrong format of email. Example: email@mail.com")

def getDate(str):
    os.system("cls")
    while True:
        dateStr = input(str)
        os.system("cls")
        try:
            date = datetime.datetime.strptime(dateStr, "%d/%m/%Y")
            if date.year > 2018:
                return dateStr
            else:
                print("Date should be later than 01/01/2019!")
        except ValueError:
            print("Wrong format of date")

def generateEmail(name, surname):
    name = name.lower()
    surname = surname.lower()
    domens = ["mail", "email", "gmail", "outlook", "yahoo", "icloud"]
    extentions = ["net", "com", "org", "ua", "ru"]
    return "{}_{}@{}.{}".format(name, surname, choice(domens), choice(extentions))
    
def numToParts(amount, max_part):
    parts = []
    while amount > 0:
        part = 0
        if(amount - max_part < 0):
            part = amount
        else:
            part = max_part
        amount -= part
        parts.append(part)
    return parts
