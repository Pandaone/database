import psycopg2
import names
from funcs import generateEmail

class Database:
    def __init__(self, *args):
        if(len(args) == 5):
            self.connection = psycopg2.connect(user = args[0],
                                                password = args[1],
                                                host = args[2], 
                                                port = args[3],
                                                database = args[4])
        else:
            self.connection = psycopg2.connect(user = "postgres",
                                                password = "Artem1011",
                                                host = "127.0.0.1",
                                                port = "5432",
                                                database = "lab2")
        self.cursor = self.connection.cursor() 
        self.connection.autocommit = True 

    def __del__(self):
        if 'self.connection' in locals():
            self.cursor.close()
            self.connection.close()      

    def insertTeacher(self, teacher):
        query = "INSERT INTO public.teachers (name, age, subject) VALUES (%s,%s,%s);"
        record = (teacher.name, teacher.age, teacher.subject)
        self.cursor.execute(query, record) 

    def deleteTeacherById(self, teacherId):
        query = ("DELETE FROM public.teachers WHERE id = %s;" % teacherId)
        self.cursor.execute(query)
        count = self.cursor.rowcount

        query = ("UPDATE public.groups SET curator_id = NULL "
                "WHERE curator_id = %s;"  % teacherId)
        self.cursor.execute(query)

        if count == 1:
            return True
        else:
            return False

    def updateTeacherById(self, teacher):
        query = ("UPDATE public.teachers "
                "SET name = %s, age = %s, subject = %s "
                "WHERE id = %s;")
        record = (teacher.name, teacher.age, teacher.subject, teacher.id)
        self.cursor.execute(query, record)
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False

    def insertSubject(self, subject):
        query = "INSERT INTO public.subjects (name, hours) VALUES (%s,%s);"
        record = (subject.name, subject.hours)
        self.cursor.execute(query, record)  
    
    def deleteSubjectByName(self, subjectName):
        query = ("UPDATE public.teachers "
                "SET subject = NULL "
                "WHERE subject = %s;"
                "DELETE FROM public.marks "
                "WHERE subject = %s;"
                "DELETE FROM public.subjects WHERE name = %s;")
        self.cursor.execute(query, (subjectName, subjectName, subjectName))
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False

    def updateSubjectByName(self, subject):
        query = ("UPDATE public.subjects "
                "SET hours = %s WHERE name = %s;")
        record = (subject.hours, subject.name)
        self.cursor.execute(query, record)
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False
    
    def insertStudent(self, student):
        query = "INSERT INTO public.students (name, surname, email, group_name) VALUES (%s,%s,%s,%s);"
        record = (student.name, student.surname, student.email, student.group_name)
        self.cursor.execute(query, record) 
    
    def deleteStudentById(self, studentId):
        query = ("DELETE FROM public.marks "
                "WHERE student_id = %s;"
                "DELETE FROM public.students WHERE id = %s;")   
        self.cursor.execute(query, (studentId, studentId))
        count = self.cursor.rowcount
        if count > 0:
            return True
        else:
            return False
    
    def updateStudentById(self, student):
        query = ("UPDATE public.students "
                "SET name = %s, surname = %s, email = %s, group_name = %s "
                "WHERE id = %s;")
        record = (student.name, student.surname, student.email, student.group_name, student.id)
        self.cursor.execute(query, record)
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False
    
    def insertMark(self, mark):
        query = "INSERT INTO public.marks (student_id, subject, mark, date) VALUES (%s,%s,%s,%s);"
        record = (mark.student_id, mark.subject, mark.mark, mark.date)
        self.cursor.execute(query, record)   

    def deleteMarkById(self, markId):
        query = "DELETE FROM public.marks WHERE id = %s;"
        self.cursor.execute(query, (markId, ))
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False

    def updateMarkById(self, mark):
        query = ("UPDATE public.marks "
                "SET student_id = %s, subject = %s, mark = %s, date = %s "
                "WHERE id = %s;")
        record = (mark.student_id, mark.subject, mark.mark, mark.date, mark.id)
        self.cursor.execute(query, record)
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False

    def insertGroup(self, group):
        query = "INSERT INTO public.groups (group_name, class_num, curator_id) VALUES (%s,%s,%s);"
        record = (group.group_name, group.class_num, group.curator_id)
        self.cursor.execute(query, record)      

    def deleteGroupByName(self, group_name):
        query = ("UPDATE public.students SET group_name = NULL WHERE group_name = %s;"
                "DELETE FROM public.groups WHERE group_name = %s;")
        self.cursor.execute(query, (group_name, group_name))
        count = self.cursor.rowcount
        if count > 0:
            return True
        else:
            return False

    def updateGroupByName(self, group):
        query = ("UPDATE public.groups "
                "SET class_num = %s, curator_id = %s "
                "WHERE group_name = %s;")
        record = (group.class_num, group.curator_id, group.group_name)
        self.cursor.execute(query, record)
        count = self.cursor.rowcount
        if count == 1:
            return True
        else:
            return False

    def generateStudents(self, amount = 0):
        for i in range(amount):
            name = names.get_first_name()
            surname = names.get_last_name()
            query = ("INSERT INTO public.students (name, surname, email, group_name) "
                    "VALUES (%s, %s, %s, getRandomGroup());")
            self.cursor.execute(query, (name, surname, generateEmail(name, surname), ))
    
    def generateMarks(self, amount = 0):
        query = ("INSERT INTO public.marks (student_id, subject, mark, date) "
                "SELECT getRandomStudentId(), "
                "getRandomSubject(), trunc(random() * 60 + 40) :: int, "
                "getRandomDate() FROM generate_series(1, %s);" % amount)
        self.cursor.execute(query)
        
    def getStudentsByCuratorAndName(self, subject, text):
        query = ("SELECT name, email, group_name FROM public.students "
                "WHERE group_name in (SELECT groups.group_name "
                "FROM public.groups, public.teachers WHERE groups.curator_id = teachers.id "
                "AND teachers.subject = %s) AND name LIKE %s;")
        self.cursor.execute(query, (subject, text, ))
        return self.cursor.fetchall()

    def getStudentsByTheMostMarks(self, mark1, mark2):
        query = ("WITH marks_amount AS (SELECT student_id, COUNT(*) AS cnt FROM public.marks "
                "WHERE mark BETWEEN %s AND %s GROUP BY student_id), "

                "max_marks_amount AS (SELECT student_id, cnt FROM marks_amount "
                "WHERE cnt = (SELECT MAX(cnt) FROM marks_amount)) "

                "SELECT s.name, s.email, s.group_name, m.cnt AS total "
                "FROM public.students AS s INNER JOIN max_marks_amount AS m ON "
                "m.student_id = s.id;" % (mark1, mark2))
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def getClassMarks(self, classNum):
        query = ("WITH group_marks as (SELECT s.group_name, AVG(m.mark) AS avarage "
                "FROM public.students AS s, public.marks AS m "
                "WHERE s.id = m.student_id GROUP BY s.group_name) "
                "SELECT AVG(avarage) FROM group_marks AS m, public.groups AS g "
                "WHERE g.group_name = m.group_name AND class_num = %s;" % classNum)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def getAvgMarksByStudentId(self, student_id):
        query = ("SELECT DISTINCT to_char(date, 'YYYY-MM') as date, "
                "round(AVG(mark) OVER (PARTITION BY to_char(date, 'YYYY-MM')), 2) AS average " 
                "FROM marks WHERE student_id = %s ORDER BY date;" % student_id)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def getAvgStudentMark(self, student_id):
        query = "SELECT round(AVG(mark), 2) FROM marks WHERE student_id = %s;"
        self.cursor.execute(query, (student_id, ))  
        return self.cursor.fetchall()

    def getAvgMarksBySubject(self, subject):
        query = ("SELECT DISTINCT to_char(date, 'YYYY-MM') as date, "
                "round(AVG(mark) OVER (PARTITION BY to_char(date, 'YYYY-MM')), 2) AS average "
                "FROM marks WHERE subject = '%s' ORDER BY date;" % subject)
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def getAvgSubjectMark(self, subject):
        query = "SELECT round(AVG(mark), 2) FROM marks WHERE subject = %s;"
        self.cursor.execute(query, (subject, ))  
        return self.cursor.fetchall()

    def getNumberOfStudentsInCLass(self):
        query = ("WITH get_amount AS (SELECT DISTINCT group_name, COUNT(*) as cnt FROM students GROUP BY group_name)"

                "SELECT class_num, SUM(cnt) FROM groups JOIN get_amount as amount "
                "ON (groups.group_name = amount.group_name) GROUP BY class_num;")
        self.cursor.execute(query)  
        return self.cursor.fetchall()

    def createSQLFunctions(self):
        query1 = ("CREATE OR REPLACE FUNCTION getRandomLine(start INTEGER, space INTEGER, leters_amount INTEGER) "
                "RETURNS TEXT AS $$"
                "DECLARE line TEXT;"
                "BEGIN "
                "   SELECT array_to_string(ARRAY("
                "   SELECT chr((start + trunc(random() * space)) :: int)"
                "   FROM generate_series(1, leters_amount)), '') INTO line;"
                "   RETURN line;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query1)

        query2 = ("CREATE OR REPLACE FUNCTION getRandomLine(leters_amount integer) "
                "RETURNS TEXT AS $$"
                "DECLARE"
                "   possible_chars TEXT := 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';"
                "   line TEXT;"
                "BEGIN "
                "   SELECT array_to_string(ARRAY("
                "   SELECT substring(possible_chars, (random() * length(possible_chars) + 1) :: int, 1)"
                "   FROM generate_series(1, leters_amount)), '') INTO line;"
                "   RETURN line;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query2)

        query3 = ("CREATE OR REPLACE FUNCTION getRandomGroup() "
                "RETURNS TEXT AS $$"
                "DECLARE res TEXT;"
                "BEGIN "
                "   SELECT group_name FROM public.groups ORDER BY random() LIMIT 1 INTO res;"
                "   RETURN res;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query3)

        query4 = ("CREATE OR REPLACE FUNCTION getRandomStudentId() "
                "RETURNS INTEGER AS $$"
                "DECLARE res INTEGER;"
                "BEGIN "
                "   SELECT id FROM public.students TABLESAMPLE SYSTEM_ROWS(1) INTO res;"
                "   RETURN res;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query4)

        query5 = ("CREATE OR REPLACE FUNCTION getRandomSubject() "
                "RETURNS TEXT AS $$"
                "DECLARE res TEXT;"
                "BEGIN "
                "   SELECT name FROM public.subjects ORDER BY random() LIMIT 1 INTO res;"
                "   RETURN res;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query5)

        query6 = ("CREATE OR REPLACE FUNCTION getRandomDate() "
                "RETURNS DATE AS $$"
                "DECLARE resDate TEXT;"
                "BEGIN "
                "   SELECT date '01.01.2019' + (random() * (date '01.01.2021' - '01.01.2019')) :: INTEGER INTO resDate;"
                "   RETURN resDate;"
                "END;"
                "$$ LANGUAGE PLPGSQL;")
        self.cursor.execute(query6)
