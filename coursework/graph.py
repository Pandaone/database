from matplotlib import pyplot as plt
from matplotlib import ticker
from matplotlib import dates as mdates
from datetime import datetime

def printGraph(points, avg = None):
    plt.title("Graph of average marks")
    plt.xlabel("Period(month)")
    plt.ylabel("Mark")

    ax = plt.gca()

    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=4))
    ax.xaxis.set_minor_locator(mdates.MonthLocator())

    ax.yaxis.set_major_locator(ticker.AutoLocator())
    ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())

    for i in range(len(points) - 1):
        point1 = points[i]
        point2 = points[i + 1]

        date1 = datetime.strptime(point1[0], "%Y-%m")
        date2 = datetime.strptime(point2[0], "%Y-%m")

        x_values = [date1, date2]
        y_values = [point1[1], point2[1]]

        ax.plot(x_values, y_values, color="m")

    if avg:
        avg = avg[0][0]
        ax.axhline(y=avg, color="c", label="Average mark of all period")
        ax.legend()
    
    plt.show() 

def printPieChart(data):
    names = []
    values = []
    explode = []

    for el in data:
        names.append(el[0])
        values.append(el[1])
        explode.append(0.03)
    
    plt.pie(values, labels=names, explode=explode, shadow=True, autopct='%1.1f%%')
    plt.title("Persantage of students in class")
    plt.show()
