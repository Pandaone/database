import database
import os
import funcs

from time import time
from psycopg2 import errors
from myError import MyError
from data import subjects, teachers, students, marks, groups
from multiprocessing.pool import ThreadPool

class databaseController:

    def __init__(self):
        self.database = database.Database()

    def insertTeacher(self):
        os.system("cls")
        name = funcs.getName("Enter new teacher's name: ")
        age = funcs.getPosNum("Enter teacher's age: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        teacher = teachers.Teacher(name, age, subject)
        try:
            self.database.insertTeacher(teacher)
        except errors.lookup("23503"):
            raise MyError("ERROR! No subjects in table 'subjects' like ({})".format(subject))
        except errors.lookup("22000"):
            raise MyError("ERROR! {} is too young, teacher can not be younger than 18".format(name))

    def deleteTeacherById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter teacher's id: ")
        return self.database.deleteTeacherById(id)

    def updateTeacherById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter teacher's id: ")
        name = funcs.getName("Enter new teacher's name: ")
        age = funcs.getPosNum("Enter teacher's age: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        teacher = teachers.Teacher(name, age, subject, id)
        try:
            return self.database.updateTeacherById(teacher)
        except errors.lookup("23503"):
            raise MyError("ERROR! No subjects in table 'subjects' like ({})".format(subject))

    def insertStudent(self):
        os.system("cls")
        name = funcs.getName("Enter new student's name: ")
        surname = funcs.getName("Enter new student's surname: ")
        email = funcs.getMail("Enter student's email: ")
        group = funcs.getGroupName("Enter student's group name: ")
        student = students.Student(name, surname, email, group)
        try:
            self.database.insertStudent(student)
        except errors.lookup("23503"):
            raise MyError("ERROR! No groups in table 'groups' like ({})".format(group))
        except errors.lookup("23505"):
            raise MyError("ERROR! Someone is already use this email({})".format(email))
    
    def deleteStudentById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter student's id: ")
        return self.database.deleteStudentById(id)

    def updateStudentById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter student's id: ")
        name = funcs.getName("Enter new student's name: ")
        surname = funcs.getName("Enter new student's surname: ")
        email = funcs.getMail("Enter student's email: ")
        group = funcs.getGroupName("Enter student's group name: ")
        student = students.Student(name, surname, email, group, id)
        try:
            return self.database.updateStudentById(student)
        except errors.lookup("23503"):
            raise MyError("ERROR! No groups in table 'groups' like ({})".format(group))
        except errors.lookup("23505"):
            raise MyError("ERROR! Someone is already use this email({})".format(email))

    def insertSubject(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter new subject name: ")
        hours = funcs.getPosNum("Enter subject's hours: ")
        subject = subjects.Subject(name, hours)
        try:
            self.database.insertSubject(subject)
        except errors.lookup("23505"):
            raise MyError("ERROR! This subject({}) is already exists!".format(name))
    
    def deleteSubjectByName(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter subject's name: ")
        return self.database.deleteSubjectByName(name)
    
    def updateSubjectByName(self):
        os.system("cls")
        name = funcs.getSubjectName("Enter subject name: ")
        hours = funcs.getPosNum("Enter subject's hours: ")
        subject = subjects.Subject(name, hours)
        return self.database.updateSubjectByName(subject)

    def insertGroup(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        class_num = funcs.getPosNum("Enter class number: ")
        curator = funcs.getPosNum("Enter curator's id: ")
        group = groups.Group(name, class_num, curator)
        try:
            self.database.insertGroup(group)
        except errors.lookup("23505"):
            raise MyError("ERROR! Group({}) is already exists or teacher with id({}) is already a curator!!!".format(name, curator))
        except errors.lookup("23503"):
            raise MyError("ERROR! No teacher with id - {}".format(curator))

    def deleteGroupByName(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        return self.database.deleteGroupByName(name)

    def updateGroupByName(self):
        os.system("cls")
        name = funcs.getGroupName("Enter group name: ")
        class_num = funcs.getPosNum("Enter class number: ")
        curator = funcs.getPosNum("Enter curator's id: ")
        group = groups.Group(name, class_num, curator)
        try:
            return self.database.updateGroupByName(group)
        except errors.lookup("23505"):
            raise MyError("ERROR! Teacher with id({}) is already a curator!!!".format(curator))
        except errors.lookup("23503"):
            raise MyError("ERROR! No teacher with id - {}".format(curator))

    def insertMark(self):
        os.system("cls")
        student_id = funcs.getPosNum("Enter student's id: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        mark = 101
        while mark > 100:
            mark = funcs.getPosNum("Enter mark from 1 to 100: ")
        date = funcs.getDate("Enter date {dd/mm/yyyy}: ")
        mark = marks.Mark(student_id, subject, mark, date)
        try:
            self.database.insertMark(mark)
        except errors.lookup("23503"):
            raise MyError("ERROR! No student with id({}) or no subject({})".format(student_id, subject))

    def deleteMarkById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter mark's id: ")
        return self.database.deleteMarkById(id)        

    def updateMarkById(self):
        os.system("cls")
        id = funcs.getPosNum("Enter marks's id: ")
        student_id = funcs.getPosNum("Enter student's id: ")
        subject = funcs.getSubjectName("Enter subject name: ")
        mark = 101
        while mark > 100:
            mark = funcs.getPosNum("Enter mark from 1 to 100: ")
        date = funcs.getDate("Enter date {dd/mm/yyyy}: ")
        mark = marks.Mark(student_id, subject, mark, date, id)
        try:
            return self.database.updateMarkById(mark)
        except errors.lookup("23503"):
            raise MyError("ERROR! No student with id({}) or no subject({})".format(student_id, subject))

    def generateStudents(self):
        os.system("cls")
        amount = funcs.getPosNum("Enter amount of students to be generated: ")
        parts = funcs.numToParts(amount, 5000)
        try:
            pool = ThreadPool(len(parts))
            pool.map(self.database.generateStudents, parts)
            pool.close()
            pool.join()
        except errors.lookup("23505"):
            raise MyError("ERROR! Email was used twice while generation")

    def generateMarks(self):
        os.system("cls")
        amount = funcs.getPosNum("Enter amount of marks to be generated: ")
        parts = funcs.numToParts(amount, 10000)

        pool = ThreadPool(len(parts))
        pool.map(self.database.generateMarks, parts)
        pool.close()
        pool.join()
        
    def getStudentsByCuratorAndName(self):
        os.system("cls")
        subject = funcs.getSubjectName("Enter subject name: ")
        namePat = input("Enter part of name. Example - '%hze%': ")
        time1 = time()
        students = self.database.getStudentsByCuratorAndName(subject, namePat)
        time2 = time()
        return [students, (time2 - time1) * 1000]

    def getStudentsByTheMostMarks(self):
        os.system("cls")
        mark1 = 101
        while mark1 > 100:
            mark1 = funcs.getPosNum("Enter first mark from 1 to 100: ")
        mark2 = 101
        while mark2 > 100:
            mark2 = funcs.getPosNum("Enter second mark from 1 to 100: ")
        time1 = time()
        students = self.database.getStudentsByTheMostMarks(mark1, mark2)
        time2 = time()
        return [students, (time2 - time1) * 1000]

    def getClassMarks(self):
        os.system("cls")
        class_num = funcs.getPosNum("Enter class number: ")
        time1 = time()
        mark = self.database.getClassMarks(class_num)
        time2 = time()
        return [mark, (time2 - time1) * 1000]

    def getAvgMarksByStudentId(self):
        os.system("cls")
        student_id = funcs.getPosNum("Enter student id: ")
        points = self.database.getAvgMarksByStudentId(student_id)
        avg = self.database.getAvgStudentMark(student_id)
        return [points, avg]

    def getAvgStudentMark(self):
        os.system("cls")
        student_id = funcs.getPosNum("Enter student id: ")
        return self.database.getAvgStudentMark(student_id)

    def getAvgMarksBySubject(self):
        os.system("cls")
        subject = funcs.getSubjectName("Enter subject name: ")
        points = self.database.getAvgMarksBySubject(subject)
        avg = self.database.getAvgSubjectMark(subject)
        return [points, avg]

    def getAvgSubjectMark(self):
        os.system("cls")
        subject = funcs.getSubjectName("Enter subject name: ")
        return self.database.getAvgSubjectMark(subject)

    def getNumberOfStudentsInCLass(self):
        os.system("cls")
        data = self.database.getNumberOfStudentsInCLass()
        res = []
        for el in data:
            res.append(["Class №" + str(el[0]), el[1]])
        return res
