import os
from databaseController import databaseController
from myError import MyError
from graph import printGraph, printPieChart
from PIL import Image
import csv
from predictions import predict
import re

def getCommands():
    print("1  - insert new teacher\n"
        "2  - delete teacher\n"
        "3  - update teacher\n"
        "4  - insert student\n"
        "5  - delete student\n"
        "6  - update student\n"
        "7  - insert subject\n"
        "8  - delete subject\n"
        "9  - update subject\n"
        "10 - insert group\n"
        "11 - delete group\n"
        "12 - update group\n"
        "13 - insert mark\n"
        "14 - delete mark\n"
        "15 - update mark\n"
        "16 - generate students\n"
        "17 - generate marks\n"
        "18 - get all students that's curator teach some subject and their name consists some pattern\n"
        "19 - get all students that have the most amount of marks in some interval\n"
        "20 - get average mark in some classroom\n"
        "21 - get average mark of student on every month\n"
        "22 - get average mark of subject on every month\n"
        "23 - get number of students in each class\n"
        "24 - get average mark of student\n"
        "25 - get average mark of students on particular subject\n"
        "26 - get graph of index using\n"
        "27 - predict future marks of student\n"
        "28 - get all commands\n"
        "29 - quit")

database = databaseController()

command = ""
getCommands()
while command != "29":
    command = input("Enter your command: ")
    os.system("cls")
    try:
        if command == "1":
            database.insertTeacher()
            print("Teacher was added successfully")
        elif command == "2":
            if(database.deleteTeacherById()):
                print("Teacher was deleted successfully")
            else:
                print("No teacher with such id")
        elif command == "3":
            if(database.updateTeacherById()):
                print("Teacher was updated successfully")
            else:
                print("No teacher with such id")
        elif command == "4":
            database.insertStudent()
            print("Student was added successfully")
        elif command == "5":
            if(database.deleteStudentById()):
                print("Student was deleted successfully")
            else:
                print("No student with such id")
        elif command == "6":
            if(database.updateStudentById()):
                print("Student was updated successfully")
            else:
                print("No student with such id")
        elif command == "7":
            database.insertSubject()
            print("Subject was added successfully")
        elif command == "8":
            if(database.deleteSubjectByName()):
                print("Subject was deleted successfully")
            else:
                print("No subject with such id")
        elif command == "9":
            if(database.updateSubjectByName()):
                print("Subject was updated successfully")
            else:
                print("No subject with such id")
        elif command == "10":
            database.insertGroup()
            print("Group was added successfully")
        elif command == "11":
            if(database.deleteGroupByName()):
                print("Group was deleted successfully")
            else:
                print("No group with such id")
        elif command == "12":
            if(database.updateGroupByName()):
                print("Group was updated successfully")
            else:
                print("No group with such id")
        elif command == "13":
            database.insertMark()
            print("Mark was added successfully")
        elif command == "14":
            if(database.deleteMarkById()):
                print("Mark was deleted successfully")
            else:
                print("No mark with such id")
        elif command == "15":
            if(database.updateMarkById()):
                print("Mark was updated successfully")
            else:
                print("No mark with such id")
        elif command == "16":
            database.generateStudents()
            print("Students were generated successfully")
        elif command == "17":
            database.generateMarks()
            print("Marks were generated successfully")
        elif command == "18":
            os.system("cls")
            res = database.getStudentsByCuratorAndName()
            students = res[0]
            if len(students) == 0:
                print("No students were found")
                continue
            for i in range(len(students)):
                print("Student {}:( Name - {}, Email - {}, Group - {})".format(
                        i + 1, students[i][0], students[i][1], students[i][2]))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "19":
            os.system("cls")
            res = database.getStudentsByTheMostMarks()
            students = res[0]
            if len(students) == 0:
                print("No students were found")
                continue
            for i in range(len(students)):
                print("Student {}:\nName - {}\nEmail - {}\nGroup - {}\nAmount - {}\n".format(
                        i + 1, students[i][0], students[i][1], students[i][2], students[i][3]))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "20":
            res = database.getClassMarks()
            mark = res[0][0][0]
            if not mark:
                print("No students in this classroom")
                continue
            print("The average mark is {}".format(mark))
            print("Time of query execution - {}ms".format(res[1]))
        elif command == "21":
            res = database.getAvgMarksByStudentId()
            points = res[0]
            avg = res[1]
            if points and avg:
                printGraph(points, avg)
            else:
                print("Students with such id doesn't have any marks")
        elif command == "22":
            res = database.getAvgMarksBySubject()
            points = res[0]
            avg = res[1]
            if points and avg:
                printGraph(points, avg)
            else:
                print("No subject with this name")
        elif command == "23":
            data = database.getNumberOfStudentsInCLass()
            printPieChart(data)
        elif command == "24":
            avg = database.getAvgStudentMark()[0][0]
            if avg:
                print("The average mark of student is {}".format(avg))
            else:
                print("No student with such id")
        elif command == "25":
            avg = database.getAvgSubjectMark()[0][0]
            if avg:
                print("The average mark of students on this subject is {}".format(avg))
            else:
                print("No subject with such name")
        elif command == "26":
            image = Image.open('index.png')
            image.show()
            print("Blue line displays the speed of query with index, red one - without")
        elif command == "27":
            filename = 'students.csv'
            res = database.getAvgMarksByStudentId()
            points = res[0]
            if points:
                with open(filename, 'w', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(["Mark"])
                    last_date = ""
                    for p in points:
                        writer.writerow([p[1]])
                        last_date = p[0]
                res = predict(filename)  
                marks = res[0]
                res_points = []
                month = int(last_date[5: 7:])
                for mark in marks:
                    month += 1
                    next_month = str(month).zfill(2)
                    date = re.sub(r"-\d\d", "-{}".format(next_month), last_date)
                    res_points.append([date, mark])
                printGraph(res_points)
            else:
                print("No student with such id")
        elif command == "28":
            os.system("cls")
            getCommands()
        elif command == "29":
            print("Program finished!")
        else:
            print("Wrong command")
    except MyError as err:
        print(err.text)
    except Exception as err:
        print("Unknow Error\n{}".format(err))
