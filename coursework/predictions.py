import pandas as pd
from sklearn import linear_model, metrics

def predict(filename):
    f = pd.read_csv(filename)
    for i in range(3,25):
        f.loc[i, 'mark3'] = f.loc[i-3, 'Mark']
        f.loc[i, 'mark2'] = f.loc[i-2, 'Mark']
        f.loc[i, 'mark1'] = f.loc[i-1, 'Mark']
    f.drop([0,1,2], axis=0, inplace=True)
    train_data = f.iloc[:-7, :]
    test_data = f.iloc[-7:, :]
    train_labels = train_data['Mark'].values
    train_labels.astype(float)
    train_data = train_data.drop(['Mark'], axis = 1)
    train_data.astype(float)
    test_labels = test_data['Mark'].values
    test_data = test_data.drop(['Mark'], axis = 1)
    linear_regressor = linear_model.LinearRegression()
    linear_regressor.fit(train_data, train_labels)
    predictions = linear_regressor.predict(test_data)
    err = metrics.mean_absolute_error(test_labels, predictions)
    return [predictions.tolist(), err]
